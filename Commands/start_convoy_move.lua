function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "formation", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<relative formation>",
			},
			{ 
				name = "fight",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end

function Run(self, units, parameter)
	local position = parameter.position -- Vec3
	local formation = parameter.formation -- array of Vec3
	local fight = parameter.fight -- boolean
	
	--Spring.Echo(dump(parameter.formation))
	
	-- validation
	if (#units > #formation) then
		Logger.warn("formation.move", "Your formation size [" .. #formation .. "] is smaller than needed for given count of units [" .. #units .. "] in this group.") 
		return FAILURE
	end
	
	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end
	
	
	-- create subgroups
	
	local Convoy_SG_Array = {}
	for i=1, 10 do
		Convoy_SG_Array[i]={}
		for j=1, 4 do
		Convoy_SG_Array[i][j]=0
		end
		
	end
		
	-- order the convoy in the subgroups
	
	local k = 1  --impair SG for Stump
	local m = 1
		
	local n = 2 --pair SG for corcv 
	local p = 1
		
	for i=1, #units do
	
		
		if Spring.GetUnitDefID(units[i]) == 145 then --if it's a stump
		
		
			if k == 1 then --first Stump SG has 4 elements
				
				Convoy_SG_Array[k][m]=units[i]				
				m = m + 1
				
				if m == 5 then
					m = 1
					k = k + 2
				end
			
			else 	if m == 3 then
						m = 1
						k = k + 2
					end

					Convoy_SG_Array[k][m]=units[i]
					m = m + 1
			
			end 
			
		
		else if p == 5 then --it is supposed to be corcv
				p = 1
				n = n + 2
				end

			Convoy_SG_Array[n][p]=units[i]
			p = p + 1
		
		end
		
	end
	
	--Spring.Echo(dump(Convoy_SG_Array[2][3]))
	
	-- appoint the pointman

	local pointman = Convoy_SG_Array[1][1] -- while this is running, we know that #units > 0, so pointman is valid
	local pointX, pointY, pointZ = SpringGetUnitPosition(pointman)
	local pointmanPosition = Vec3(pointX, pointY, pointZ)
	
	-- threshold of pointan success
	if (pointmanPosition == self.lastPointmanPosition) then 
		self.threshold = self.threshold + THRESHOLD_STEP 
	else
		self.threshold = THRESHOLD_DEFAULT
	end
	self.lastPointmanPosition = pointmanPosition
	
	-- Re-initialize the indexes
	
	k = 1  --used here for both types of SG
	m = 2
		
	k_old = 1 -- save the previous vehicle
	m_old = 1
	
	
	-- check pointman success
	-- THIS LOGIC IS TEMPORARY, NOT CONSIDERING OTHER UNITS POSITION
	if (pointmanPosition:Distance(position) < self.threshold) then
		return SUCCESS
	else
		SpringGiveOrderToUnit(pointman, cmdID, position:AsSpringVector(), {})
		
		for i=2, #units do
			
				local thisUnitWantedPosition = pointmanPosition + formation[i]
				SpringGiveOrderToUnit(Convoy_SG_Array[k][m], cmdID, thisUnitWantedPosition:AsSpringVector(), {})
				
				--Increment k and m
				if k == 1 then 
							
					m = m + 1
				
					if m == 5 then
						m = 1
						k = k + 1
					end
				end
				
				if (k%2)==0 then
				
					m = m + 1
				
					if m == 5 then
						m = 1
						k = k + 1
					end
				
				else m = m + 1
				
					if m == 3 then
						m = 1
						k = k + 1
					end
				
				end
				
				--Increment k_old and m_old
				if k_old == 1 then 
							
					m_old = m_old + 1
				
					if m_old == 5 then
						m_old = 1
						k_old = k_old + 1
					end
				end
				
				if (k_old%2)==0 then
				
					m_old = m_old + 1
				
					if m_old == 5 then
						m_old = 1
						k_old = k_old + 1
					end
				
				else m_old = m_old + 1
				
					if m_old == 3 then
						m_old = 1
						k_old = k_old + 1
					end
				
				end
				
		end
		
	end
		
		return RUNNING
end


function Reset(self)
	ClearState(self)
end
