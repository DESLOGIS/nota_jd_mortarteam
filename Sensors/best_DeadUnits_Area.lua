function getInfo()
	return {
		fields = { "pos" }
	}
end

local function tablelength(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end

local function distancePyth(p1, p2)
	return math.sqrt((p1.x - p2.x)^2 + (p1.z - p2.z)^2)
end

local function distanceToLine(p1, p2, p3)

	local numerateur = math.abs((p1.z - p2.z)*p3.x - (p1.x - p2.x)*p3.z + p1.x*p2.z - p2.x*p1.z)
	local denumerateur = math.sqrt((p1.x - p2.x)^2 + (p1.z - p2.z)^2)

	return numerateur / denumerateur
end

local function length(v1)

  return math.sqrt(v1.x * v1.x + v1.z * v1.z)

end

local function normalize(v1)

  local len = length(v1)

  v1.x = v1.x / len

  v1.z = v1.z / len

  return self

end

return function(posPointMan, nextMinePos)
	
	local halfWidth = 150
	local halfHeitgh = 150
	
	local metalValue = 1
	local movingCost = 0
	local fromPathCost = 0
	
	local maxScore = 0
	local maxScoreIndice = 1
	
	local areasChracteristics = {}
	
	local directionVector = nextMinePos - posPointMan
	local norDirectionVector = directionVector:Normalize()
	local nbrAreaToMine = math.floor(directionVector:Length() / 2*halfWidth)
	
	for i = 1, nbrAreaToMine do
	
		for j = 1, 3 do
		
			local xloc = 2*i*halfWidth
			local zloc = 2*j*halfHeitgh
			local yloc = Spring.GetGroundHeight(xloc, zloc)
			local center = posPointMan + Vec3(xloc, yloc, zloc) * norDirectionVector
			
			local scoreArea = 0
			
			local listFeaturesArea = Spring.GetFeaturesInRectangle(center.x-halfWidth, center.z-halfHeitgh, center.x+halfWidth, center.z+halfHeitgh)
			
			for k = 1, tablelength(listFeaturesArea) do 
			
				local remainingMetal,_,_,_,_ = Spring.GetFeatureResources(listFeaturesArea[k])
				scoreArea = scoreArea + remainingMetal
			
			end
			
			local areaScore = metalValue * scoreArea + movingCost * center:Distance(posPointMan) + fromPathCost * distanceToLine(posPointMan, nextMinePos, center)
			areasChracteristics[i+3*(j-1)] = center
			
			if areaScore > maxScore then
			
				maxScore = areaScore
				maxScoreIndice = i+3*(j-1)
				
			end 
			
		end
	end
	
	--Spring.Echo(dump(maxScore), dump(areasChracteristics[maxScoreIndice]))
	
	if maxScore > 0 then 
		return areasChracteristics[maxScoreIndice]
	else 
		return nil
	end
end