local sensorInfo = {
	name = "HighestPos",
	desc = "Return the highest position in an area",
	author = "Superjojo91",
	date = "2017-04-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end



return function(position, radius)
	local HighestPos = position
	
	for i=-5, 5 do
		for j=-5, 5 do
		
			local x = position.x+i*(radius/10)
			local z = position.z+j*(radius/10)
		
			local height = Spring.GetGroundHeight(x,z)
			if (height > HighestPos.y) then
				HighestPos =Vec3(x,height,z)
			end
		end
	end
	
	return HighestPos
end